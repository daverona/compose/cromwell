workflow DockerHelloWorld {
  call Greet {
    input:
      message = "Hello, World!",
  }
}

task Greet {
  String message
  command {
    echo ${message}
  }
  runtime {
    docker: "ubuntu:20.04"
  }
}
