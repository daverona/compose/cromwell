workflow Main {
  call Hello {
    input:
      name = "World",
  }
  output {
    String message = Hello.message
  }
}

task Hello {
  String name
  command {
    echo "Hello, ${name}!"
  }
  output {
    String message = read_string(stdout())
  }
}
