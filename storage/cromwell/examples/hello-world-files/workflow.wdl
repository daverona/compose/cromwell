workflow HelloWorldFiles {
  File hello_file
  String world
  call Greet {
    input:
      hello_file = hello_file,
      world = world,
  }
}

task Greet {
  File hello_file
  String world
  String greeting_filename = "greeting.txt"
  command {
    echo $(cat "${hello_file}")", ${world}!" >> "${greeting_filename}"
  }
  output {
    File greeting_file = "${greeting_filename}"
  }
}
