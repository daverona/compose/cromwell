workflow MyWorkflow {
  String workflow_indir
  String workflow_outdir
  String workflow_dir
  call MyTask {
    input:
      input_dir = workflow_indir,
      output_dir = workflow_outdir,
      model_dir = workflow_dir
  }
}

task MyTask {
  String input_dir
  String output_dir
  String model_dir
  command {
    echo "Hello, inputs" >> "${output_dir}"/listing.txt
    ls -al "${input_dir}" >> "${output_dir}"/listing.txt
    echo "Hello, models" >> "${output_dir}"/listing.txt
    ls -al "${model_dir}" >> "${output_dir}"/listing.txt
  }
  output {
    File out = "${output_dir}"
  }
  runtime {
    docker: "ubuntu:20.04"
    docker_opts:
      "--volume " + input_dir + ":" + input_dir + ":ro"
      + " --volume " + output_dir + ":" + output_dir
      + " --volume " + model_dir + ":" + model_dir + ":ro"
  }
}
