import "sub-workflow.wdl" as SubWorkflow

workflow MainHelloWorkflow {
  call SubWorkflow.HelloWorkflow {
    input: name = "stranger"
  }
  output {
    String output = HelloWorkflow.hello_message
  }
}
