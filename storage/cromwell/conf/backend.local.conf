# Rename to backend.local.conf and set the following variables:
local.remote-user = ${BACKEND_USERNAME}
local.remote-host = ${BACKEND_HOSTNAME}
local.remote-port = ${BACKEND_SSH_PORT}
local.cromwell-root = cromwell-executions

###############################################################################
# Do NOT edit after this line unless you know what you are doing.
###############################################################################

# Predefined variables in cromwell:
# ${job_name} -- a unique name for the job
# ${script} -- a shell script of the job to be run
# ${cwd} -- the path where the script should be run
# ${out} -- the path the stdout
# ${err} -- the path the stderr
# ${docker} -- the docker image name
# ${docker_script} -- the path of the ${script} within the docker container
# ${docker_cwd} -- the path where ${cwd} should be mounted within the docker container
# ${docker_out} -- the path of the ${out} within the docker container
# ${docker_err} -- the path of the ${err} within the docker container
# ${docker_cid} -- the host path to which the container ID file should be written

# References
# @see https://github.com/broadinstitute/cromwell/blob/master/cromwell.example.backends/LocalExample.conf
# @see https://github.com/broadinstitute/cromwell/blob/master/core/src/main/resources/reference.conf
# @see https://github.com/broadinstitute/cromwell/blob/master/core/src/main/resources/reference_local_provider_config.inc.conf
# @see https://cromwell.readthedocs.io/en/stable/Configuring/#job-shell-configuration
# @see https://cromwell.readthedocs.io/en/stable/tutorials/Containers/#docker-root

local {
  actor-factory = "cromwell.backend.impl.sfs.config.ConfigBackendLifecycleActorFactory"
  config {
    # @see https://github.com/broadinstitute/cromwell/blob/master/core/src/main/resources/reference_local_provider_config.inc.conf
    include required(classpath("reference_local_provider_config.inc.conf"))
    include required(file("/usr/local/java/conf/filesystems.conf"))

    root = ${local.cromwell-root}
    job-shell = /bin/bash
    run-in-background = true

    runtime-attributes = """
      Int? gpus
      String? docker
      String? docker_opts
      String docker_user = """"${CROMWELL_UID}""":"""${CROMWELL_GID}""""
    """

    submit-docker = """
      known_hosts=$(mktemp /tmp/tmp.known_hosts.XXXXXXXXXX) \
      && ssh-keyscan -p """${local.remote-port}""" -H """${local.remote-host}""" > $(echo $known_hosts) \
      && ssh -o UserKnownHostsFile=$(echo $known_hosts) -p """${local.remote-port}" "${local.remote-user}"@"${local.remote-host}""" '/bin/bash --login -c "\
        rm -rf "${docker_cid}" \
        && docker container run \
          --interactive \
          --env TZ="""${TZ}""" \
          --cidfile ${docker_cid} \
          --user ${docker_user} \
          ${"--gpus " + gpus} \
          ${docker_opts} \
          --volume ${cwd}:${docker_cwd}:delegated \
          --entrypoint ${job_shell} \
          ${docker} \
            ${docker_script} \
          > ${out} \
          2> ${err} \
        && (cat "${docker_cid}" | xargs docker wait) > /dev/null \
        && rc=$? \
        && (cat "${docker_cid}" | xargs docker rm) > /dev/null \
        && exit $rc \
      "' \
      && rm -rf $(echo $known_hosts)
    """

    kill-docker = """
      known_hosts=$(mktemp /tmp/tmp.known_hosts.XXXXXXXXXX) \
      && ssh-keyscan -p """${local.remote-port}""" -H """${local.remote-host}""" > $(echo $known_hosts) \
      && ssh -o UserKnownHostsFile=$(echo $known_hosts) -p """${local.remote-port}" "${local.remote-user}"@"${local.remote-host}""" '/bin/bash --login -c "\
        docker kill \`cat ${docker_cid}\` \
      "' \
      && rm -rf $(echo $known_hosts)
    """
  }
}
