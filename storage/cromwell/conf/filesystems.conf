# References
# @see https://cromwell.readthedocs.io/en/stable/backends/HPC/#shared-filesystem
# @see https://cromwell.readthedocs.io/en/stable/Configuring/#local-filesystem-options

filesystems {
  local {
    localization: [ "hard-link", "soft-link", "copy" ]
    caching: {
      # When copying a cached result, what type of file duplication should occur.
      # possible values: "hard-link", "soft-link", "copy", "cached-copy".
      # For more information check: https://cromwell.readthedocs.io/en/stable/backends/HPC/#shared-filesystem
      # Attempted in the order listed below:
      duplication-strategy: [ "hard-link", "soft-link", "copy" ]

      # Possible values: md5, xxh64, fingerprint, path, path+modtime
      # For extended explanation check: https://cromwell.readthedocs.io/en/stable/Configuring/#call-caching
      # "md5" will compute an md5 hash of the file content.
      # "xxh64" will compute an xxh64 hash of the file content. Much faster than md5
      # "fingerprint" will take last modified time, size and hash the first 10 mb with xxh64 to create a file fingerprint.
      # This strategy will only be effective if the duplication-strategy (above) is set to "hard-link", as copying changes the last modified time.
      # "path" will compute an md5 hash of the file path. This strategy will only be effective if the duplication-strategy (above) is set to "soft-link",
      # in order to allow for the original file path to be hashed.
      # "path+modtime" will compute an md5 hash of the file path and the last modified time. The same conditions as for "path" apply here.
      # Default: "md5"
      hashing-strategy: "md5"

      # When the 'fingerprint' strategy is used set how much of the beginning of the file is read as fingerprint.
      # If the file is smaller than this size the entire file will be read.
      # Default: 10485760 (10MB).
      fingerprint-size: 10485760

      # When true, will check if a sibling file with the same name and the .md5 extension exists, and if it does, use the content of this file as a hash.
      # If false or the md5 does not exist, will proceed with the above-defined hashing strategy.
      # Default: false
      check-sibling-md5: false
    }  # caching
  }  # local
} # filesystems
