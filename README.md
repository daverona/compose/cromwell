# daverona/compose/cromwell

## Prerequisites

* `cromwell.example` mapped to `127.0.0.1` &mdash; check [this](https://gitlab.com/daverona/compose/dnsmasq) out
* [Traefik](https://docs.traefik.io/) &mdash; check [this](http://gitlab.com/daverona/compose/traefik) out

## Quick Start

Make `.env` file:

```bash
cp .env.example .env  
# edit .env
```

Make configuration files under `storage/cromwell/conf`:

```bash
cd storage/cromwell/conf
cp app.conf.example app.conf  # required
cp database.conf.example database.conf  # only if you plan to use mariadb
cp local.conf.example local.conf  # only if you plan to use local backend
cp slurm.conf.example slurm.conf  # only if you plan to use slurm backend
# note that you need at least one backend
# edit *.conf files
cd -
```

To use local backend, 
do the following (after set the value of `LOCAL_ADDRESS`):

```bash
LOCAL_ADDRESS=host.example

# Make cromwell log in without password to localhost
docker container run --rm \
  daverona/cromwell \
    cat /home/cromwell/.ssh/id_rsa.pub \
>> ~/.ssh/authorized_keys

# Make cromwell trust localhost
docker container run --rm \
  daverona/cromwell \
    ssh-keyscan -H ${LOCAL_ADDRESS} \
>> ./storage/cromwell/conf/known_hosts 2>/dev/null

unset LOCAL_ADDRESS
```

To use slurm backend, 
do the following (after set the values of `SLURM_ADDRESS` and `SLURM_USERNAME`):

```bash
SLURM_ADDRESS=slurmctld.example
SLURM_USERNAME=tom

# Make cromwell log in without password to slurm host
docker container run --rm \
  daverona/cromwell \
    cat /home/cromwell/.ssh/id_rsa.pub \
| ssh ${SLURM_USERNAME}@${SLURM_ADDRESS} 'cat >> .ssh/authorized_keys'
# enter your password if asked

# Make cromwell trust slurm host
docker container run --rm \
  daverona/cromwell \
    ssh-keyscan -H ${SLURM_ADDRESS} \
>> ./storage/cromwell/conf/known_hosts 2>/dev/null

# Repeat with numeric address
SLURM_ADDRESS=192.168.xx.xx

# Make cromwell trust slurm host
docker container run --rm \
  daverona/cromwell \
    ssh-keyscan -H ${SLURM_ADDRESS} \
>> ./storage/cromwell/conf/known_hosts 2>/dev/null

unset SLURM_ADDRESS
unset SLURM_USERNAME
```

Finally run cromwell container:

```bash
docker-compose up --detach
```

Wait until cromwell is up and visit [http://cromwell.example](http://cromwell.example).

## References

* Cromwell Documentation: [https://cromwell.readthedocs.io/en/stable/](https://cromwell.readthedocs.io/en/stable/)
* Cromwell repository: [https://github.com/broadinstitute/cromwell](https://github.com/broadinstitute/cromwell)
* biowdl/BamMetrics: [https://github.com/biowdl/BamMetrics](https://github.com/biowdl/BamMetrics)
